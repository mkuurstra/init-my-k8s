from invoke import task

@task
def sync(c):
    c.run("pipenv sync", pty=True)
    c.run("pipenv run ansible-galaxy install -fr requirements.yml", pty=True)

@task
def lint(c):
    c.run("pipenv run -- yamllint --strict argo-cd playbooks roles .yamllint.yml inventory.yml requirements.yml", pty=True)

@task
def test(c):
    c.run("echo 'No test available'", pty=True)

@task(test)
def deploy(c):
    c.run("pipenv run ansible-playbook playbooks/main.yml", pty=True)
