# Longhorn

## Secret for backup account

```bash
LONGHORN_S3_ACCESS_KEY_ID=""
LONGHORN_S3_SECRET_ACCESS_KEY=""
LONGHORN_S3_URL=""

kubectl -n longhorn-system create secret generic cloud-backup \
    --from-literal=AWS_ACCESS_KEY_ID="$LONGHORN_S3_ACCESS_KEY_ID" \
    --from-literal=AWS_SECRET_ACCESS_KEY="$LONGHORN_S3_SECRET_ACCESS_KEY" \
    --from-literal=AWS_ENDPOINTS="$LONGHORN_S3_URL" \
    --dry-run=client -o yaml \
  | kubeseal --format yaml \
  | pbcopy
```
