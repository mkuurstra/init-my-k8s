# Velero

## Secret for backup account

```bash
echo "[default]
aws_access_key_id=<AWS_ACCESS_KEY_ID>
aws_secret_access_key=<AWS_SECRET_ACCESS_KEY>
" > /tmp/velero.tmp

kubectl -n velero create secret generic velero-credentials \
    --from-file=cloud=/tmp/velero.tmp \
    --dry-run=client -o yaml \
  | kubeseal --format yaml \
  | pbcopy
```
