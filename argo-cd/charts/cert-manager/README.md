# Cert-Manager

## Cloudflare token

```bash
CLOUDFLARE_TOKEN=""

kubectl -n cert-manager create secret generic cloudflare \
    --from-literal=cloudflare-token="$CLOUDFLARE_TOKEN" \
    --dry-run=client -o yaml \
  | kubeseal --format yaml \
  | pbcopy
```
