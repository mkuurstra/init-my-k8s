# Argo CD

## Argo CD Image Updater

### Credentials

```bash
GITHUB_USER=""
GITHUB_TOKEN=""

kubectl -n argo-cd create secret generic git-creds-init-my-k8s \
    --from-literal=username="$GITHUB_USER" \
    --from-literal=password="$GITHUB_TOKEN" \
    --dry-run=client -o yaml \
  | kubeseal --format yaml \
  | pbcopy
```
