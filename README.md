# Init my Kubernetes

This projects (clears and) deploys a Kubernetes cluster using Ansible, kubelet and kubeadm and installs Argo CD as a way of bootstrapping the cluster and maintaining our cluster in sync with the git repository.

- [Init my Kubernetes](#init-my-kubernetes)
  - [Overview](#overview)
  - [Requirements](#requirements)
  - [Setup](#setup)
    - [Environment variables](#environment-variables)
    - [1Password Connect Kubernetes Operator](#1password-connect-kubernetes-operator)

## Overview

The goal is a Kubernetes cluster with all basic management tools to aid a Kubernetes cluster for home use on bare-metal. To achieve this the following software is used:

- **Networking**: Calico
- **Continuous Delivery**: Argo CD
- **Storage**: Longhorn
- **Load-balancer**: MetalLB
- **Reverse proxy**: Traefik
- **Backup**: Velero and built-in Longhorn capabilities
- **Off-site backup**: S3 capable API (e.g. AWS / Backblaze / Minio)
- **Secret management**: 1Password Connect Kubernetes Operator
- **DNS**: Pi-hole
- **Certificate management**: cert-manager / Cloudflare / Lets Encrypt

Missing features that should be added:

- Log management
- Monitoring (Prometheus?)
- Visualization (Grafana?)
- Expand to allow multiple controllers

Ansible is only needed for the initial setup and when destroying en redeploying.

I'd say Continuous Integration is missing from the list above but im intentionally excluding this for now. I'm using Gitlab runners for this but i'm expecting users to have other opinions or simply not needing CI for now.

## Requirements

Ansible host:

- macOS (probably Linux or WSL will work too)
- Python 3.10 or later
- Ansible 2.13 or later

Kubernetes cluster:

- Ubuntu 22.04 LTS

## Setup

### Environment variables

Create a .env file with the following contents:

```bash
PIHOLE_ADMIN_PSW=
```

#### General

Stub

#### Cloudflare

Stub

#### MetalLB

Stub

#### S3 buckets

Stub

#### Pi-hole

Stub

### 1Password Connect Kubernetes Operator

Stub
