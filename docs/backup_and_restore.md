# Backup and Restore

We backup and restore storage volumes using the backup capabilities of `Longhorn` and for everything else we use `Velero`.

Both these services backup to a S3 capable API. So for example AWS of coure but a self-hosted `minio` is possible as well as using the services of `Backblaze B2`. 

## Backup

* Install Velero client: <https://velero.io/docs/main/basic-install/#install-the-cli>
* Velero makes use of the current `kubectl` context, make sure you're using the correct context.
* Start backup ([Inspiration from here](https://longhorn.github.io/longhorn-tests/manual/pre-release/cluster-restore/restore-to-a-new-cluster/))

```bash
velero backup create mini-cluster --exclude-resources persistentvolumes,persistentvolumeclaims,backuptargets.longhorn.io,backupvolumes.longhorn.io,backups.longhorn.io,nodes.longhorn.io,volumes.longhorn.io,engines.longhorn.io,replicas.longhorn.io,backingimagedatasources.longhorn.io,backingimagemanagers.longhorn.io,backingimages.longhorn.io,sharemanagers.longhorn.io,instancemanagers.longhorn.io,engineimages.longhorn.io
```

## Restore

* Setup initial cluster using this project
* Restore secrets and certificates resources for the whole cluster:

```bash
velero restore create --from-backup mini-cluster --include-resources secrets,certificates
```

* Restore all resources for the whole cluster except for essential namespaces that are managed by this project.

```bash
velero restore create --from-backup mini-cluster --exclude-namespaces longhorn-system,metallb-system,traefik-system,velero
```

* Go to Longhorn portal 
* Restore volumes from backup
* Create PVCs and PVs with previous names for the restored volumes
